{
    "id": "e01979b4-bf72-4cf1-bf8a-13c84863160d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprHole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bf210ae-64c6-4e3c-bbd9-55c4bcae8bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e01979b4-bf72-4cf1-bf8a-13c84863160d",
            "compositeImage": {
                "id": "65def34c-7dde-46c3-b6b0-3dfd6dc83592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf210ae-64c6-4e3c-bbd9-55c4bcae8bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db03caf4-10f0-4762-b6db-12f973b54eb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf210ae-64c6-4e3c-bbd9-55c4bcae8bca",
                    "LayerId": "fc25ece3-01d2-4086-8700-3b2cb5373e5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc25ece3-01d2-4086-8700-3b2cb5373e5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e01979b4-bf72-4cf1-bf8a-13c84863160d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}