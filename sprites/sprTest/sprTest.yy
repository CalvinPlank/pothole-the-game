{
    "id": "7f7c3893-6592-43dc-86cb-bcc4fe263c41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 63,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c643bf34-193c-43d1-8e3e-143d42c1b8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f7c3893-6592-43dc-86cb-bcc4fe263c41",
            "compositeImage": {
                "id": "5c7e96de-75d3-495b-93e7-d9d2de1b8fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c643bf34-193c-43d1-8e3e-143d42c1b8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cdab70-f9cd-4097-a2ea-4bf285b8e9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c643bf34-193c-43d1-8e3e-143d42c1b8ec",
                    "LayerId": "1404e83b-0f71-4251-a514-221686069712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1404e83b-0f71-4251-a514-221686069712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f7c3893-6592-43dc-86cb-bcc4fe263c41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}