{
    "id": "2359aa1a-f3c9-44bd-8146-a72062bf3e60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRoad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22974189-8a60-4d49-a434-39849d466c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2359aa1a-f3c9-44bd-8146-a72062bf3e60",
            "compositeImage": {
                "id": "e03cb746-2258-463d-b916-d7bf5002b7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22974189-8a60-4d49-a434-39849d466c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1617030b-ccb0-4884-acb4-0e19a340026f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22974189-8a60-4d49-a434-39849d466c05",
                    "LayerId": "449d141c-9c08-4547-83e9-70d4acd8f627"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "449d141c-9c08-4547-83e9-70d4acd8f627",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2359aa1a-f3c9-44bd-8146-a72062bf3e60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}