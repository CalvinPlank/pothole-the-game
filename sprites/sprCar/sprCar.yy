{
    "id": "f06d827e-01b3-4407-8a3a-3269a005af8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f87f47fe-398d-474d-8c0b-073475e11a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f06d827e-01b3-4407-8a3a-3269a005af8c",
            "compositeImage": {
                "id": "ed263b05-0c41-49e8-9ebe-7a2323258227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f87f47fe-398d-474d-8c0b-073475e11a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344985af-9ea3-461f-beab-1dbfbeb7be09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f87f47fe-398d-474d-8c0b-073475e11a8b",
                    "LayerId": "446a47c5-0cd7-4b5c-90ea-7bafc7cb3bb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "446a47c5-0cd7-4b5c-90ea-7bafc7cb3bb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f06d827e-01b3-4407-8a3a-3269a005af8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}