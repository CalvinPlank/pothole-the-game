{
    "id": "e397aa4c-5d5b-4b6a-bdd4-960e885da7ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPothole",
    "eventList": [
        {
            "id": "8a5f6842-34b6-46b1-98d7-9dcccda8fcb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e397aa4c-5d5b-4b6a-bdd4-960e885da7ea"
        },
        {
            "id": "98364f4e-ef36-4269-b40a-dbdf4730700d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e397aa4c-5d5b-4b6a-bdd4-960e885da7ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e01979b4-bf72-4cf1-bf8a-13c84863160d",
    "visible": true
}