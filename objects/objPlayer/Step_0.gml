if(!global.fastness == 0) { turnSpeed = 10/(global.fastness + 2); }	// to avoid divide by zero error
leftPressed = keyboard_check(vk_left);
rightPressed = keyboard_check(vk_right);

if(rightPressed) { global.angle-=turnSpeed; }
if(leftPressed) { global.angle+=turnSpeed; }

if(leftPressed && rightPressed) {
	if(global.fastness - brakeSpeed > 0) { global.fastness -= brakeSpeed; }
	else global.fastness = 0;
} else {
	if(global.fastness + acceleration < maxSpeed) { global.fastness += acceleration; }
	else global.fastness = maxSpeed;
}

image_angle = global.angle + 90;