{
    "id": "87d53a7e-efc4-45d9-ac8a-37d791d854d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objRoad",
    "eventList": [
        {
            "id": "88fe385b-2497-4f5f-9449-15a3d1f0016c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "87d53a7e-efc4-45d9-ac8a-37d791d854d2"
        },
        {
            "id": "7ab73d7d-37fb-4e2f-bc70-fa43f863e3bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87d53a7e-efc4-45d9-ac8a-37d791d854d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "78fd7fb2-f250-4804-9ea4-18a1085bc479",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2359aa1a-f3c9-44bd-8146-a72062bf3e60",
    "visible": true
}