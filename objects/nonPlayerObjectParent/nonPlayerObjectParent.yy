{
    "id": "78fd7fb2-f250-4804-9ea4-18a1085bc479",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "nonPlayerObjectParent",
    "eventList": [
        {
            "id": "33824486-f58f-4b5e-a6c7-29fbfb7c1ea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78fd7fb2-f250-4804-9ea4-18a1085bc479"
        },
        {
            "id": "011ae498-c4b6-43a0-945c-cdf3e7bbe5f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "78fd7fb2-f250-4804-9ea4-18a1085bc479"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f7c3893-6592-43dc-86cb-bcc4fe263c41",
    "visible": true
}